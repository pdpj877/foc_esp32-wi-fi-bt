# 双路 FOC无刷电机控制 with BT&Wifi

## 硬件部分

+ 8-18V供电工作电压
+ CP2102串口&Type-C
+ 自复位烧录电路集成
+ 双路Drv8313无刷电机驱动器 & 两路IIC编码器通道
+ 带电源输入电压采集功能
+ 板载 Wifi & BlueTooth
+ 板载 5V1A SH1.0接口输出
+ 板载 UART&ADC采集接口
  
## 固件部分

+ 使用PlatformIO + vscode开发
+ 引脚配置在Config.h中
+ 已经移植simpleFOC库，可自行调用
+ 例程带
  - 长按按键复位功能
  - 串口通信基本指令解析线程
  - 蓝牙通信线程

## 软件部分

+ 暂无