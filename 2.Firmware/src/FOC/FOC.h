#ifndef __FOC__
#define __FOC__
#include <Arduino.h>
#include "FOC_DEF.h"

namespace FOC{
    void init(void);
    void Update(void);
    void LOG(void);

    void encoderInit(void);
    bool getEncoderPos(void);
    void getMotorSpeed(void);
    
    void motorInit(void);
    void setCH1Speed(int speed);
    void setCH2Speed(int speed);
    void CH1SpeedCtrl(void);
    void CH2SpeedCtrl(void);
    void setTwoChannelSpeed(int CH1_speed,int CH2_speed);
    void changeTone(int Tone);
}
#endif