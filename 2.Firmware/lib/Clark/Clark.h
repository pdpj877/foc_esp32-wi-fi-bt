#ifndef __CLARK__
#define __CLARK__
#include <Arduino.h>
#include "math.h"
float iClarkAlphaBetaToA(float alpha,float beta);
float iClarkAlphaBetaToB(float alpha,float beta);
float iClarkAlphaBetaToC(float alpha,float beta);

#endif