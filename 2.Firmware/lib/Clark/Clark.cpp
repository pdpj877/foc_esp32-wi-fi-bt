#include "Clark.h"
#include <Arduino.h>
#include "math.h"

float iClarkAlphaBetaToA(float alpha,float beta){
    return alpha;
}
float iClarkAlphaBetaToB(float alpha,float beta){
    return -1/2.0*alpha + sqrtf(3.0)/2 * beta;
}
float iClarkAlphaBetaToC(float alpha,float beta){
    return -1/2.0*alpha - sqrtf(3.0)/2 * beta;
}